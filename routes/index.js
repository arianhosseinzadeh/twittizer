var express = require('express');
var router = express.Router();
//var tweetAnalyzer = require('../app/tweetAnalyzer');
var getUserTweets = require('../app/getUserTweets');
var getUserInfo = require('../app/getUserInfo');
var getUserFollowers = require('../app/getFollowers');
var getUserFollowings = require('../app/getFollowings');
var userInfoReport = require('../app/userInfoReport');
var userTweetReport = require('../app/userTweetReport');
router.get('/user_tweets/:username', function(req, res, next) {
    getUserTweets(req.params.username).then(function(result) {
        console.log(result);
        console.log(result.length);
        res.send(result);
    });
});

router.get('/user_info/:username', function(req, res, next) {
    getUserInfo(req.params.username).then(function(result) {
        console.log(result);
        res.send(result);
    });
});

router.get('/user_followers/:username', function(req, res, next) {
    getUserFollowers(req.params.username).then(function(result) {
        console.log(result);
        console.log(result.length);
        res.send(result);
    });
});

router.get('/user_followings/:username', function(req, res, next) {
    getUserFollowings(req.params.username).then(function(result) {
        console.log(result);
        console.log(result.length);
        res.send(result);
    });
});

router.get('/user/:username', function(req, res, next) {
/*    userInfoReport(req.params.username).then(function(result){
        console.log(result);
        res.send(result);
    });*/
    userTweetReport(req.params.username).then(function(result){
        //console.log(result);
        res.send(result);
    });
});
/*router.get('/', function(req, res, next) {
 res.render('index');
 });*/
/*

 router.get('/hashtag/:hashtag', function(req, res, next) {
 tweetAnalyzer(req.params.hashtag).then(function(result) {
 res.send(JSON.stringify(result));
 });
 });
 */

module.exports = router;
