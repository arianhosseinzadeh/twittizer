var cynetwork = require('./../app/network');

var network = new cynetwork();
var nodes = ['iran', 'irandeal', 'no2rouhani', 'istandwithisrael', 'peace', 'isupportirandeal'];
var added_nodes = [];

var eventHandlers = {
    dijkstra: [
        {
            'event': 'tap',
            'action': function(cy, selected_nodes) {
                return function(evt) {
                    if (this.hasClass('selected-node')) {
                        selected_nodes.splice(selected_nodes.indexOf(this.id()), 1);
                        this.addClass('unselected-node')
                            .removeClass('selected-node');
                    } else {
                        if (selected_nodes.length !== 2) {
                            var other_node_id = selected_nodes[0];
                            selected_nodes.push(this.id());
                            this.addClass('selected')
                                .removeClass('unselected');
                            if (selected_nodes.length === 2) {
                                var root_id = this.id();
                                var dijkstra = cy.elements().dijkstra('#' + root_id);
                                var path = dijkstra.pathTo('#' + other_node_id);
                                path.forEach(function(ele) {
                                    ele.addClass('colored');
                                    if (ele.isNode()) {
                                        ele.style({
                                            'border-width': 4,
                                            'border-color': 'green'
                                        });
                                    }
                                    if (ele.isEdge()) {
                                        ele.style({
                                            'line-color': 'green',
                                            'line-style': 'dashed'
                                        });
                                    }
                                });
                                cy.$(':selected').forEach(function(ele) {
                                    ele.removeClass('selected-node')
                                        .addClass('unselected-node');
                                });
                            }
                        }
                    }
                }
            }
        }
    ]
};
var randGen = function(n) {
    return Math.floor(Math.random() * n);
};

while (nodes.length !== 0) {
    var rand_index = randGen(nodes.length - 1);
    var node_id = nodes.splice(rand_index, 1) + '';
    added_nodes.push(node_id);
    network.addNode(node_id, eventHandlers.dijkstra);
}

var edgeMap = {};
var number_of_edges = randGen(added_nodes.length * (added_nodes.length - 1) / 2);
nodes = added_nodes;
var i = 0;

while (i < number_of_edges) {
    var sourceId = nodes[randGen(nodes.length)];
    var targetId = nodes[randGen(nodes.length)];
    var edgeId = sourceId + '' + targetId;
    if (!edgeMap[edgeId]) {
        edgeMap[edgeId] = true;
        network.addEdge(edgeId, sourceId, targetId);
        i++;
    }
}

/*var betweenness = network.getCy().elements().betweennessCentrality(function() {
    return 1;
}, false).betweennessNormalized;


nodes.forEach(function(nodeId) {
    console.log(nodeId + ' : ' + betweenness('#' + nodeId));
});*/


console.log('------------');
network.getClosenessCentrality();
/*
var ccn = network.getCy().$().ccn();
console.log(ccn.closeness('#iran'));
nodes.forEach(function(nodeId) {
    console.log(nodeId + ' : ' + closeness('#' + nodeId));
});*/
