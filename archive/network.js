var cytoscape = require('cytoscape');

var network = function() {
    var cy = cytoscape({
        container: document.getElementById('container'),
        layout: {
            name: 'circle'
        },
        style: [
            {
                selector: 'node',
                style: {
                    'content': 'data(id)'
                }
            },
            {
                selector: 'edge',
                style: {
                    'content': 'data(id)',
                    width: 10,
                    'curve-style': 'bezier',
                    'line-color': 'red'
                }
            }
        ]
    });
    var node_spec =
    {
        selected: false, // whether the element is selected (default false)

        selectable: true, // whether the selection state is mutable (default true)

        locked: false, // when locked a node's position is immutable (default false)

        grabbable: true, // whether the node can be grabbed and moved by the user

        //classes: 'foo bar', // a space separated list of class names that the element has

        // NB: you should only use `style`/`css` for very special cases; use classes instead
        style: {
            'background-color': 'red'
        } // overriden style properties
    };
    var concatJSONs = function(biggerJSON, smallerJSON) {
        for (var key in smallerJSON) {
            if (smallerJSON.hasOwnProperty(key)) {
                biggerJSON[key] = smallerJSON[key];
            }
        }
        return biggerJSON;
    };
    var selected_nodes = [];
    var addNode = function(id, eventListenerMap) {
        var node = cy.add({
            group: "nodes",
            data: concatJSONs({
                id: id
            }, node_spec)
        }).addClass('unselected-node');
        eventListenerMap.forEach(function(eventHandler) {
            node.on(eventHandler.event, eventHandler.action(cy, selected_nodes));
        });
    };
    var addEdge = function(edgeId, sourceId, targetId) {
        cy.add({
                group: "edges",
                data: {
                    id: edgeId,
                    source: sourceId,
                    target: targetId
                }
            }
        )
    };
    var getCy = function() {
        return cy;
    };
    var getClosenessCentrality = function(){
        var ccn = cy.$().ccn({});
        console.log(ccn.closeness('#iran'));
    };
    return ({
        addEdge: addEdge,
        addNode: addNode,
        getCy: getCy,
        getClosenessCentrality: getClosenessCentrality
    });
};
module.exports = network;