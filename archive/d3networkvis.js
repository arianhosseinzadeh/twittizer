/*
var d3 = require('d3');
var width = 1000,
    height = 1000;
var nodeMaxFreq = -1;
var nodeMinFreq = Number.POSITIVE_INFINITY;
var linkMaxFreq = -1;
var linkMinFreq = Number.POSITIVE_INFINITY;

var graph = {
    "nodes": [
        {"name": "A0", "freq": 100, "polarity": -1, "query_hashtag": true},
        {"name": "A1", "freq": 89, "polarity": -0.9},
        {"name": "A2", "freq": 30, "polarity": 0.7},
        {"name": "A3", "freq": 10, "polarity": -0.8},
        {"name": "A4", "freq": 20, "polarity": 1},
        {"name": "A5", "freq": 2, "polarity": 0}
    ],
    "links": [
        {"source": 0, "target": 1, "freq": 30},
        {"source": 0, "target": 2, "freq": 20},
        {"source": 1, "target": 2, "freq": 10},
        {"source": 1, "target": 3, "freq": 10},
        {"source": 1, "target": 4, "freq": 0},
        {"source": 1, "target": 5, "freq": 0},
        {"source": 2, "target": 5, "freq": 0}
    ]
};

var concatJSONs = function(biggerJSON, smallerJSON) {
    for (var key in smallerJSON) {
        if (smallerJSON.hasOwnProperty(key)) {
            biggerJSON[key] = smallerJSON[key];
        }
    }
    return biggerJSON;
};
var prepareNodes = function() {
    return graph.nodes.map(function(d, index) {
        var r = Math.sqrt((Math.floor(Math.random()) + 1) * -Math.log(Math.random())) * 12;
        var node_setting = {
            radius: r,
            x: d["query_hashtag"] ? width / 2 : 200 * Math.cos(index * 2 * Math.PI / ( graph.nodes.length - 1)) + (width / 2),
            y: d["query_hashtag"] ? height / 2 : 200 * Math.sin(index * 2 * Math.PI / (graph.nodes.length - 1)) + (height / 2)
        };
        return concatJSONs(d, node_setting);
    });
};

var nodes = prepareNodes();

var links = graph.links;

var updateNodeFreq = function() {
    nodes.map(function(d) {
        nodeMaxFreq = Math.max(nodeMaxFreq, d.freq);
        nodeMinFreq = Math.min(nodeMinFreq, d.freq);
    });
};
var updateLinkFreq = function() {
    links.map(function(d) {
        linkMaxFreq = Math.max(linkMaxFreq, d.freq);
        linkMinFreq = Math.min(linkMinFreq, d.freq);
    });
};
updateNodeFreq();
updateLinkFreq();

var force = d3.layout.force()
    .charge(function(d) {
        var charge = -10000;
        if (d.index === 0) charge = 10 * charge;
        return charge;
    })
    .size([width, height]) // size of the svg
    .gravity(1); // gravity of the center of the clone to the center of the svg

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

force
    .nodes(nodes)
    .links(links)
    .start();

var link = svg.selectAll(".link")
    .data(links)
    .enter().append("line")
    .attr("class", "link");

var node = svg.selectAll(".node")
    .data(nodes)
    .enter().append("g")
    .attr("class", "node");

node.append("text")
    .text(function(d) {
        return d.name
    });

function collide(node) {
    var r = node.radius + 16,
        nx1 = node.x - r,
        nx2 = node.x + r,
        ny1 = node.y - r,
        ny2 = node.y + r;
    return function(quad, x1, y1, x2, y2) {
        if (quad.point && (quad.point !== node)) {
            var x = node.x - quad.point.x,
                y = node.y - quad.point.y,
                l = Math.sqrt(x * x + y * y),
                r = node.radius + quad.point.radius;
            if (l < r) {
                l = (l - r) / l * .5;
                node.x -= x *= l;
                node.y -= y *= l;
                quad.point.x += x;
                quad.point.y += y;
            }
        }
        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
    };
}

force.on("tick", function() {
    link.
        attr("x1", function(d) {
            return d.source.x;
        })
        .attr("y1", function(d) {
            return d.source.y;
        })
        .attr("x2", function(d) {
            return d.target.x;
        })
        .attr("y2", function(d) {
            return d.target.y;
        });
    var q = d3.geom.quadtree(nodes),
        i = 0,
        n = nodes.length;

    while (++i < n) q.visit(collide(nodes[i]));

    node.attr("transform", function(d, index) {
        return "translate(" + d.x + "," + d.y + ")";
    });
});


setTimeout(function() {
    graph.nodes.push(
        {"name": "A6", "freq": 27, "polarity": 1}
    );
    links.push(
        {"source": 3, "target": 1, "freq": 70}
    );
    nodes = prepareNodes();
    links = graph.links;
    console.log(nodes);
    force.resume();
    node = node.data(nodes)
        .enter().append("g")
        .attr("class", "node");
    node.append("text")
        .text(function(d) {
            return d.name
        })
        .call(force.drag);
    link = link.data(links)
        .enter().append("line")
        .attr("class", "link")
        .call(force.drag);
}, 3000);
*/
