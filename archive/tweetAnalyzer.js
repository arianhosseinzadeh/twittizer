var Twit = require('twit');
var clusterfck = require('clusterfck');
var Promise = require('Promise');
var MAX_PAGE_SIZE = 100;
var sentiment = require('sentiment');
/* GET home page. */
var fs = require('fs');

var queryTweet = function(T, query, since, until, tweetCount, cursorCount, max_id) { //cursorCount and max_id are for
    //internal uses of this function as it's recursive and it wants to keep state
    return new Promise(function(resolve, reject) {
        T.get('search/tweets', {
            q: '#' + query + ' since:' + since + ' until:' + until + (max_id ? ' max_id:' + max_id : ''),
            count: MAX_PAGE_SIZE
        }, function(err, data, response) {
            console.log(query + ':' + ((cursorCount / MAX_PAGE_SIZE ) || 0));
            cursorCount = cursorCount ? cursorCount + MAX_PAGE_SIZE : MAX_PAGE_SIZE;
            if (data && data.statuses && cursorCount < tweetCount && data.statuses.length === MAX_PAGE_SIZE) {
                var min_id = data.statuses[0].id;
                data.statuses.map(function(tweet) {
                    min_id = Math.min(min_id, tweet.id)
                });
                queryTweet(T, query, since, until, tweetCount, cursorCount, min_id).then(function(result) {
                    resolve(result.concat(data.statuses));
                });
            } else {
                resolve(data.statuses);
            }
        })
    });
};
var findHashTags = function(tweet) {
    var hashTagSet = {};
    tweet.entities.hashtags.map(function(hashTag) {
        hashTagSet[hashTag.text.toLowerCase()] = true;
    });
    var result = [];
    for (var hashTag in hashTagSet) {
        if (hashTagSet.hasOwnProperty(hashTag)) {
            result.push(hashTag);
        }
    }
    return result;
};
var createTweetObject = function(tweet) {
    var tweetText;
    if (!!tweet.retweeted_status) {
        tweetText = tweet.retweeted_status.text
    } else {
        tweetText = tweet.text;
    }
    return {
        text: tweetText,
        userLocation: tweet.user.location,
        userFollowersCount: tweet.user['followers_count'],
        userFriendsCount: tweet.user['friends_count'],
        userStatusesCount: tweet.user['statuses_count'],
        tweetDate: tweet['created_at'],
        userJoinDate: tweet.user['created_at'],
        retweetCount: tweet['retweet_count'],
        hashtags: findHashTags(tweet)
    }
};

var transformTweetsList = function(tweetArray) {
    var result = {};
    tweetArray.map(function(tweet) {
        result[tweet.id] = createTweetObject(tweet);
    });
    return result;
};
var concatJSONs = function(biggerJSON, smallerJSON) {
    for (var key in smallerJSON) {
        if (smallerJSON.hasOwnProperty(key)) {
            biggerJSON[key] = smallerJSON[key];
        }
    }
    return biggerJSON;
};
var sortHashTagsByCount = function(tweets) {
    var hashTagCount = {};
    tweets.map(function(tweet) {
        var hashTags = findHashTags(tweet);
        hashTags.map(function(hashTag) {
            if (hashTagCount[hashTag]) {
                hashTagCount[hashTag] = hashTagCount[hashTag] + 1;
            } else {
                hashTagCount[hashTag] = 1;
            }
        });
    });
    var hashTagCountArray = [];
    for (var hashTag in hashTagCount) {
        if (hashTagCount.hasOwnProperty(hashTag)) {
            hashTagCountArray.push({hashtag: hashTag, count: hashTagCount[hashTag]});
        }
    }
    hashTagCountArray.sort(function(a, b) {
        return b.count - a.count;
    });
    return hashTagCountArray;
};
var selectTopHashTags = function(tweetArray, blackListSet, topK) {
    var selectedCount = 0;
    var tweetIndex = 0;
    var topTweets = [];
    while (tweetIndex < tweetArray.length && selectedCount < topK) {
        if (blackListSet && !blackListSet[tweetArray[tweetIndex].hashtag]) {
            topTweets.push(tweetArray[tweetIndex].hashtag);
            selectedCount++;
        }
        tweetIndex++;
    }
    return topTweets;
};
var arrayToJSON = function(arr) {
    var r = {};
    arr.map(function(element) {
        r[element] = true;
    });
    return r;
};
var getTweets = function(T, loopCount, topK, queries, since, until, tweetCount, hashTagSet, tweetList) {
    return new Promise(function(resolve, reject) {
        var query = queries[0];
        queryTweet(T, query, since, until, tweetCount).then(function(data) {
            //console.log('Done');
            concatJSONs(tweetList, transformTweetsList(data)); //piggyback the second json to the first object
            var hashTagCountArray = sortHashTagsByCount(data);
            var topHashTags = selectTopHashTags(hashTagCountArray, hashTagSet, topK);
            queries = queries.concat(topHashTags);
            concatJSONs(hashTagSet, arrayToJSON(topHashTags));
            queries.shift();
            //console.log(loopCount);
            if (loopCount <= 0 || queries.length === 0) {
                resolve({hashtags: hashTagSet, tweets: tweetList});
            } else {
                getTweets(T, loopCount - 1, topK, queries, since, until, tweetCount, hashTagSet, tweetList).then(function(result) {
                    resolve(result);
                });
            }
        });
    });
};
var saveToFile = function(outputFilename, data) {
    fs.writeFile(outputFilename, JSON.stringify(data, null, 4), function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log("JSON saved to " + outputFilename);
        }
    });
};
var analyzeTweets = function(hashTag) {
    return new Promise(function(resolve, reject) {
        var T = new Twit({
            consumer_key: 'lhiokAf5pkSbeGJd3TIm81Ta8'
            , consumer_secret: 'sD8Uo478BBJerlLqFYNDur2JBFKFu95ccd4ojiuBqVxJYB5Y1l'
            , access_token: '475560452-SWZvpaqB7PgvmyD6vK5qHNX8HTef5l8kzsomcLVr'
            , access_token_secret: '9fFaWYyAvFV4ptuvobOxL5keczGIEftWLDDgPQGqOygJD'
        });
        /*T.post('statuses/update', { status: req.params.message }, function(err, data, response) {
         console.log(req.params.message);
         console.log(data);
         });*/
        var hashTagSet = {};
        hashTagSet[hashTag] = true;
        var queries = [hashTag];
        var topK = 2;
        var depth = 1;
        var loopCount = depth * topK;
        var tweetCount = 100;
        var since = '2015-04-01';
        var until = '2015-09-01';
        var tweetList = {};
        getTweets(T, loopCount, topK, queries, since, until, tweetCount, hashTagSet, tweetList).then(function(result) {
            //console.log('+++++++++');
            //console.log(result.hashtags);
            //console.log(result.hashtags);
            var hashTagCount = {};
            var emotion = {};
            for (var x in tweetList) {
                if (tweetList.hasOwnProperty(x)) {
                    var validHashTags = [];
                    tweetList[x].hashtags.map(function(h) {
                        if (result.hashtags[h]) {
                            validHashTags.push(h);
                        }
                    });
                    validHashTags.sort();
                    var h = validHashTags.sort().join();
                    if (!hashTagCount[h]) {
                        hashTagCount[h] = 0;
                    }
                    if (!emotion[h]) {
                        emotion[h] = 0;
                    }
                    emotion[h] = sentiment(tweetList[x].text).score + ((hashTagCount[h] * emotion[h]) / (hashTagCount[h] + 1));
                    console.log(tweetList[x].text);
                    console.log(sentiment(tweetList[x].text).score + ' : ' + sentiment(tweetList[x].text).comparative);
                    hashTagCount[h] = hashTagCount[h] + 1;
                }
            }
            var emotion_count = [];
            for (var d in emotion) {
                if (emotion.hasOwnProperty(d)) {
                    emotion_count.push({hashtag: d, emotion: emotion[d], count: hashTagCount[d]});
                }
            }
            emotion_count.sort(function(a, b) {
                return b.count - a.count;
            });
            /*        var kmeans = new clusterfck.Kmeans();
             var clusters = kmeans.cluster(counts, 3);*/
            //console.log(Object.keys(tweetList).length);
            //console.log(hashTagCount);
            //console.log(emotion);
            //console.log(result);
            saveToFile('emotion_count.json', emotion_count);
            resolve(emotion_count);
        });
    });
};

module.exports = analyzeTweets;