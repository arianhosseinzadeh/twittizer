var getUserTweets = require('./getUserTweets');
var Promise = require('Promise');
var moment = require('moment-timezone');
var twitterTimeFormat = 'ddd MMM D HH:mm:ss p YYYY';
var MAX_PAGE_SIZE = 200;

var getUnixTimestamp = function(time_str, utc_offset) {
    console.log(time_str);
    console.log(utc_offset);
    return moment(time_str, twitterTimeFormat).utcOffset(utc_offset).unix();
};
var getOriginalURL = function(url) {
    var dotLastIndex = url.lastIndexOf('.');
    var underlineLastIndex = url.lastIndexOf('_');
    var firstPart = url.substring(0, underlineLastIndex);
    var secondPart = url.substring(dotLastIndex);
    return firstPart + '' + secondPart;
};
var getMediaURL = function(tweet){
    var result = [];
    if (!!tweet['entities'].media) {
        tweet['entities'].media.forEach(function(m){
            result.push(m['media_url']);
        });
    } else if (!!tweet['extended_entities'].media) {
        tweet['extended_entities'].media.forEach(function(m){
            result.push(m['media_url']);
        });
    }
    return result;
};
var tweetReport = function(userName) {
    return new Promise(function(resolve, reject) {
        getUserTweets(userName, 17).then(function(data) {
            var tweets = [];
            var media = [];
            var result = {};
            console.log(data);
            var index = 0;
            data.forEach(function(tweet) {
                console.log(index);
                index++;
                var t = {};
                t['time'] = getUnixTimestamp(tweet['created_at'], tweet.user['utc_offset']);
                t['retweet'] = !!tweet['retweeted'];
                t['quoted_status'] = !!tweet['quoted_status'];
                if (t['retweet']) {
                    t['text'] = tweet['retweeted_status']['text'];
                    t['retweeted_user_screen_name'] = tweet['retweeted_status'].user['screen_name'];
                    t['retweeted_user_name'] = tweet['retweeted_status'].user['name'];
                } else {
                    t['text'] = tweet['text']; // must be cleaned
                    t['retweet_count'] = tweet['retweet_count'];
                    t['favorite_count'] = tweet['favorite_count'];
                }
                t['reply_to'] = tweet['in_reply_to_screen_name'];
                t['hashtags'] = tweet.entities.hashtags;
                t['lang'] = tweet['lang'];
                t['media'] = (!!tweet['entities'] && !!tweet['entities'].media) ||
                                (!!tweet['extended_entities'] && !!tweet['extended_entities'].media) ||

                tweets.push(t);
                if (t['media']){
                    var img = {};
                    img['time'] = t['time'];
                    if (t['retweet']) {
                        img['url'] = getMediaURL(tweet['retweeted_status']);
                    } else if (t['quoted_status']){
                        img['url'] = getMediaURL(tweet['quoted_status']);
                    } else {
                        img['url'] = getMediaURL(tweet);
                    }
                    media.push(img);
                }
            });
            result['media'] = media;
            result['tweets'] = tweets;
            console.log(result);
            resolve(result);
        })
    });
};

module.exports = tweetReport;