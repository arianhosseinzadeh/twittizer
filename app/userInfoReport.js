var getUserInfo = require('./getUserInfo');
var Promise = require('Promise');
var moment = require('moment-timezone');
var twitterTimeFormat = 'ddd MMM D HH:mm:ss p YYYY';
var getUnixTimestamp = function(time_str, utc_offset) {
    //var t = moment('Fri Jan 27 05:55:14 +0000 2012', 'ddd MMM D HH:mm:ss p YYYY');
    /*    console.log(t.format("YYYY-MM-DD"));
     console.log(t.format('dddd')); // weekday
     console.log(t.format('A')); // AM/PM
     console.log(t.format('H')); // hour : 0, 1, ... ,23
     console.log(t.format('s')); // second: 0,1,...,59
     console.log(t.format('m')); // minute : 0,1,...,59*/
    return moment(time_str, twitterTimeFormat).utcOffset(utc_offset).unix();
};
var getOriginalURL = function(url) {
    var dotLastIndex = url.lastIndexOf('.');
    var underlineLastIndex = url.lastIndexOf('_');
    var firstPart = url.substring(0, underlineLastIndex);
    var secondPart = url.substring(dotLastIndex);
    return firstPart + '' + secondPart;
};
var userReport = function(userName) {
    return new Promise(function(resolve, reject) {
        getUserInfo(userName).then(function(data) {
            var result = {};
            result['screen_name'] = data['screen_name'];
            if (!!data['location']) {
                result['location'] = data['location'];
            }
            result['name'] = data['name'];
            result['description'] = data['description'];
            result['external_link'] = data['url'];
            result['followers_count'] = data['followers_count'];
            result['followings_count'] = data['friends_count'];
            result['statuses_count'] = data['statuses_count'];
            result['language'] = data['lang'];
            result['join_time'] = getUnixTimestamp(data['created_at'], data['utc_offset']);
            if (!data['default_profile_image']) {
                result['profile_image'] = getOriginalURL(data['profile_image_url']);
            }
            if (data.status) {
                result['last_tweet_time'] = getUnixTimestamp(data.status['created_at'], data['utc_offset']);
            }
            result['suspended'] = data['suspended'];
            resolve(result);
        })
    });
};

module.exports = userReport;