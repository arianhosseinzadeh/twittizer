var Twit = require('twit');
var Promise = require('Promise');
var MAX_PAGE_SIZE = 5;

var T = new Twit({
    consumer_key: 'lhiokAf5pkSbeGJd3TIm81Ta8'
    , consumer_secret: 'sD8Uo478BBJerlLqFYNDur2JBFKFu95ccd4ojiuBqVxJYB5Y1l'
    , access_token: '475560452-SWZvpaqB7PgvmyD6vK5qHNX8HTef5l8kzsomcLVr'
    , access_token_secret: '9fFaWYyAvFV4ptuvobOxL5keczGIEftWLDDgPQGqOygJD'
});

var queryTweet = function(T, userName, tweetCount, max_id) { //since_id are for
    //internal uses of this function as it's recursive and it wants to keep state
    var page_size = Math.min(tweetCount, MAX_PAGE_SIZE);
    return new Promise(function(resolve, reject) {
        if (page_size > 0) {
            var query = {
                screen_name: userName,
                count: page_size,
                include_rts: 1,
                include_entities: true
            };
            if (!!max_id) {
                query['max_id'] = max_id;
            }
            T.get('statuses/user_timeline',
                query,
                function(err, data, response) {
                    var new_max_id = Number.MAX_VALUE;
                    data.forEach(function(t) {
                        new_max_id = Math.min(t.id, new_max_id);
                    });
                    if (new_max_id === max_id) {
                        resolve([]);
                    }else if (data && data.length !== 0) {
                        queryTweet(T, userName, tweetCount - data.length, new_max_id).then(function(result) {
                            resolve(result.concat(data));
                        });
                    } else {
                        resolve(data);
                    }
                })
        } else {
            resolve([]);
        }
    });
};

var getTweets = function(userName, tweetCount) {
    return new Promise(function(resolve, reject) {
        queryTweet(T, userName, tweetCount).then(function(result) {
            resolve(result);
        });
    });
};

module.exports = getTweets;