var Twit = require('twit');
var Promise = require('Promise');
var MAX_PAGE_SIZE = 5;

var T = new Twit({
    consumer_key: 'lhiokAf5pkSbeGJd3TIm81Ta8'
    , consumer_secret: 'sD8Uo478BBJerlLqFYNDur2JBFKFu95ccd4ojiuBqVxJYB5Y1l'
    , access_token: '475560452-SWZvpaqB7PgvmyD6vK5qHNX8HTef5l8kzsomcLVr'
    , access_token_secret: '9fFaWYyAvFV4ptuvobOxL5keczGIEftWLDDgPQGqOygJD'
});

var queryFollowings = function(T, userName, followingsCount, cursor) { //since_id are for
    //internal uses of this function as it's recursive and it wants to keep state
    cursor = cursor || -1;
    var page_size = Math.min(followingsCount, MAX_PAGE_SIZE);
    return new Promise(function(resolve, reject) {
        if (page_size > 0) {
            var query = {
                screen_name: userName,
                cursor: cursor,
                count: page_size
            };
            T.get('friends/list',
                query,
                function(err, data, response) {
                    var next_cursor = data['next_cursor_str'];
                    if (data && data.users && next_cursor !== '0') {
                        queryFollowings(T, userName, followingsCount - data.users.length, next_cursor).then(function(result) {
                            resolve(result.concat(data.users));
                        });
                    } else {
                        resolve(data.users);
                    }
                })
        } else {
            resolve([]);
        }
    });
};

var getFollowings = function(userName) {
    return new Promise(function(resolve, reject) {
        queryFollowings(T, userName, 17).then(function(result) {
            resolve(result);
        });
    });
};

module.exports = getFollowings;