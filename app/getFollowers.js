var Twit = require('twit');
var Promise = require('Promise');
var MAX_PAGE_SIZE = 5;

var T = new Twit({
    consumer_key: 'lhiokAf5pkSbeGJd3TIm81Ta8'
    , consumer_secret: 'sD8Uo478BBJerlLqFYNDur2JBFKFu95ccd4ojiuBqVxJYB5Y1l'
    , access_token: '475560452-SWZvpaqB7PgvmyD6vK5qHNX8HTef5l8kzsomcLVr'
    , access_token_secret: '9fFaWYyAvFV4ptuvobOxL5keczGIEftWLDDgPQGqOygJD'
});

var queryFollowers = function(T, userName, followersCount, cursor) { //since_id are for
    //internal uses of this function as it's recursive and it wants to keep state
    cursor = cursor || -1;
    var page_size = Math.min(followersCount, MAX_PAGE_SIZE);
    return new Promise(function(resolve, reject) {
        if (page_size > 0) {
            var query = {
                screen_name: userName,
                cursor: cursor,
                count: page_size
            };
            T.get('followers/ids',
                query,
                function(err, data, response) {
                    var next_cursor = data['next_cursor_str'];
                    if (data && data.ids && next_cursor !== '0') {
                        queryFollowers(T, userName, followersCount - data.ids.length, next_cursor).then(function(result) {
                            resolve(result.concat(data.ids));
                        });
                    } else {
                        resolve(data.ids);
                    }
                })
        } else {
            resolve([]);
        }
    });
};

var getFollowers = function(userName) {
    return new Promise(function(resolve, reject) {
        queryFollowers(T, userName, 27).then(function(result) {
            resolve(result);
        });
    });
};

module.exports = getFollowers;